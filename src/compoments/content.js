import styled from 'styled-components';

const Content = styled.div`
    width: clamp(10px, 80vw, 900px);
    margin: auto;
    display: flex;
    justify-content:center;
    flex-wrap: wrap;
    margin-top: 40px;
`

export default Content;