import styled from "styled-components";

const Paragraph = styled.p`
    color: #707070;
    padding: 15px 0px;
`;

export default Paragraph;