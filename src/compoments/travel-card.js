import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import './travel-card.scss';

const Button = styled.button`
    background: transparent;
    border: none;
    padding: 10px 20px;
    cursor: pointer;
    color: #707070;
    & a{
        color: #707070;
        text-decoration: none;
    }
`;

const Title = styled.span`
    margin: 0px 0px 20px 0px;
    font-size: 1.6rem;
    text-align: center;
    color: #000;
`;

const Name = styled.span`
    margin: 10px 0px 0px 0px;
    font-size: 0.8rem;
    text-align: center;
`;

const Content = styled.span`
    margin: 0px 0px 20px 0px;
    text-align: center;
`;

const Bar = styled.div`
    margin: 0px 0px 20px 0px;
    height: 2px;
    background: #707070;
    width: 90px;
    border-radius: 5px;
`;

function TravelCard({ button, onClick, title, name, content, image, alt}) {
    return (
        <div className='travel-card'>
            <img className='travel-card-image' alt={alt} src={image} width='100%' />
            <Name>{name}</Name>
            <Title>{title}</Title>
            <Bar />
            <Content>{content}</Content>
            <Button onClick={onClick}>{button}</ Button>
        </div>
    );
}

TravelCard.propTypes = {
    button: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    onClick: PropTypes.func,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    image: PropTypes.string,
    alt: PropTypes.string
};

export default TravelCard;