import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import './lifestyle-card.scss';

const Button = styled.button`
    background: transparent;
    border: 1px solid white;
    padding: 10px 20px;
    cursor: pointer;
    color: white;
    & a{
        color: white;
        text-decoration: none;
    }
`;

const Title = styled.span`
    margin: 0px 0px 20px 0px;
    font-size: 1.6rem;
    text-align: center;
`;

const Name = styled.span`
    font-size: 0.8rem;
    text-align: center;
`;

function LifestyleCard({button, onClick, title, name, image}){
    const imageName = `url("${image}")`;

    return(
        <div className='lifestyle-card' style={{backgroundImage:imageName}}>
            <div className='lifestyle-card-iner'>
                <Name>{name}</Name>
                <Title>{title}</Title>
                <Button onClick={onClick}>{button}</ Button>
            </div>
        </div>
    );
}

LifestyleCard.propTypes = {
    button: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    onClick: PropTypes.func,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
    image: PropTypes.string
};

export default LifestyleCard;