import React from 'react';
import RandomImg from '../compoments/untils/get-random-img';
import Content from '../compoments/content';
import TravelCard from '../compoments/travel-card';
import { Link } from "react-router-dom";

function spawnCards(){
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((index)=>{
        return(
            <TravelCard name='TRAVEL' title='Charming Evening Field' button={<Link to="/articledetail">Read More</Link>}  image={RandomImg()} key={index}
                content='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula ullamcorper ex pretium volutpat. Sed quis magna est. Curabitur cursus luctus tincidunt. '/>
        );
    })
}

function ArticleList() {
    return (
        <>
            <Content>
                <div>{spawnCards()}</div>
                <div>{spawnCards()}</div>
                <div>{spawnCards()}</div>
            </Content>
        </>
    );
}

export default ArticleList;