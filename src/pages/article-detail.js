import React from 'react';
import Panel from '../compoments/panel';
import Content from '../compoments/content';
import RandomImg from '../compoments/untils/get-random-img';
import Paragraph from '../compoments/typography/paragraph';

function ArticleDetail() {
    return (
        <>
            <Panel type='large'><h1>As a Designer, I resufe to Call People 'Users'</h1></Panel>
            <Content>
                <img alt='' src={RandomImg()} width='100%' />
                <Paragraph>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum vestibulum suscipit. Aliquam ac viverra neque. Nunc placerat mauris vitae velit ullamcorper faucibus. Duis ut tellus a ante suscipit faucibus vestibulum elementum arcu. Nam a blandit tortor, in cursus nulla. Duis et viverra dolor. Donec a faucibus arcu. Donec luctus nibh eros, rhoncus dictum ligula tincidunt et. Nulla vehicula, tortor vitae condimentum consectetur, turpis augue tristique sem, vel ullamcorper risus nunc vel quam. Fusce lacinia bibendum dolor. Quisque vel iaculis velit. Aliquam in lacus vulputate, commodo est ut, dignissim eros. Pellentesque laoreet magna vitae urna viverra facilisis.
                </Paragraph>
                <Paragraph>
                    Duis pharetra aliquam elit, sed pellentesque risus. Cras lobortis nunc nec sem accumsan, ut laoreet elit malesuada. Nullam mattis purus et orci volutpat, at fringilla elit molestie. Etiam rutrum ultricies magna, eget congue arcu dictum quis. Nullam ullamcorper metus libero, ut blandit urna lacinia nec. Nulla tempus metus sapien, in auctor justo volutpat non. Morbi pulvinar eget metus ut maximus.
                </Paragraph>
                <Panel><h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum vestibulum suscipit. Aliquam ac viverra neque. Nunc placerat mauris vitae velit ullamcorper faucibus. Duis ut tellus a ante suscipit faucibus vestibulum elementum arcu. Nam a blandit tortor, in cursus nulla. </h3></Panel>
                <img alt='' src={RandomImg()} width='100%' />
                <Paragraph>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum vestibulum suscipit. Aliquam ac viverra neque. Nunc placerat mauris vitae velit ullamcorper faucibus. Duis ut tellus a ante suscipit faucibus vestibulum elementum arcu. Nam a blandit tortor, in cursus nulla. Duis et viverra dolor. Donec a faucibus arcu. Donec luctus nibh eros, rhoncus dictum ligula tincidunt et. Nulla vehicula, tortor vitae condimentum consectetur, turpis augue tristique sem, vel ullamcorper risus nunc vel quam. Fusce lacinia bibendum dolor. Quisque vel iaculis velit. Aliquam in lacus vulputate, commodo est ut, dignissim eros. Pellentesque laoreet magna vitae urna viverra facilisis.
                </Paragraph>
            </Content>
        </>
    );
}

export default ArticleDetail;